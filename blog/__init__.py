from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

app = Flask(__name__)
app.config['SECRET_KEY'] = '8fc91b10a0a20c05b30d557246892e578fadbd1afc1977cb'
app.config['SQLALCHEMY_DATABASE_URI'] ='mysql+pymysql://c21042103:Azaan010918@csmysql.cs.cf.ac.uk:3306/c21042103_C21042103_flask_blog'

db = SQLAlchemy(app)

SQLALCHEMY_TRACK_MODIFICATIONS = False

login_manager = LoginManager()
login_manager.init_app(app)

from blog import routes

from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from blog.models import User, Post, Comment
admin = Admin(app, name='Admin panel', template_mode='bootstrap3')
admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(Post, db.session))
admin.add_view(ModelView(Comment, db.session))
